***********
* README: *
***********

INTRODUCTION:
------------
This module provides an taxonomy terms export in csv format for migration.

REQUIREMENTS:
-------------


INSTALLATION:
-------------
1. Goto your Drupal site, path: sites/all/modules.

2. Enable the "Taxonomy export csv" module by navigating to:

     administer > modules


CONFIGURATION:
--------------

Goto configuration URL : admin/config/system/migrate_taxonomy

Features:
---------


Author:
-------
Vishal Sirsodiya
vishal.sirsodiya23@gmail.com
https://www.drupal.org/u/vishalsirsodiya
